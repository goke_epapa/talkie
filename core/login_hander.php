<?php
require_once("./user_auth.php");
session_start();
$jsonData = array();
if(isset($_POST['email'], $_POST['password'])){
    $email = $_POST['email'];
    $password = $_POST['password'];
    $data = login($email,$password);
    if(!is_array($data)){
        $err = "Invalid email or password, please try again.";
        $_SESSION['error_message'] = $err;
        $jsonData['error_message'] = $err;
        $jsonData['status'] = false;
    }else{
        $_SESSION['user'] = $data;
        $jsonData['user'] = $data;
        $jsonData['status'] = true;
    }
}else{
    $err = "Please enter your email address and password :)";
    $_SESSION['error_message'] = $err;
    $jsonData['error_message'] = $err;
    $jsonData['status'] = false;
}
if(isset($_POST['mobile'])){
    echo json_encode($jsonData);
    exit();
}
if(isset($_SESSION['user'])){
    header("Location: ../home.php");
}else{
    header("Location: ../");
}