<?php

require_once("./user_auth.php");
session_start();
$jsonData = array();
if(isset($_POST['email'], $_POST['password'], $_POST['c_password'], $_POST['nickname'], $_POST['lastname'], $_POST['firstname'])){
    $c_password = $_POST['c_password'];
    $nickname = $_POST['nickname'];
    $lastname = $_POST['lastname'];
    $firstname = $_POST['firstname'];
    $password = $_POST['password'];
    $fullname = $lastname . " " . $firstname;
    $email = $_POST['email'];
    if($c_password != $password){
        $err = "Passwords don't match :(";
        $_SESSION['error_message'] = $err;
        $jsonData['error_message'] = $err;
        $jsonData['status'] = false;
    }else{
        $data = signup($email,$password,$nickname, $fullname);
        if($data == true){
            $user = getUser($email, $password);
            $_SESSION['user'] = $user;
            $jsonData['user'] = $user;
        }
        else if($data == false){
            $err = "An error occured while trying to sign you up, please try again. :(";
            $_SESSION['error_message'] = $err;
            $jsonData['error_message'] = $err;
            $jsonData['status'] = false;
        }else if($data == -1){
            $err = "This email has already been used on our system, try another.";
            $_SESSION['error_message'] = $err;
            $jsonData['error_message'] = $err;
            $jsonData['status'] = false;
        }
    }
}else{
    $err = "Please enter your email address and password :)";
    $_SESSION['error_message'] = $err;
    $jsonData['error_message'] = $err;
    $jsonData['status'] = false;
}
if(isset($_POST['mobile'])){
    echo json_encode($jsonData);
    exit();
}
if(isset($_SESSION['user'])){
    header("Location: ../home.php");
}else{
    header("Location: ../signup.php");
}