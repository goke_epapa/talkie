<?php
session_start();
if (isset($_SESSION['user'])) {
    header("Location: ./home.php");
}
if (isset($_SESSION['error_message'])) {
    $error_message = $_SESSION['error_message'];
    session_destroy();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Talkie</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./css/sticky-footer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Wrap all page content here -->
<div id="wrap">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./">Talkie</a>
            </div>
            <div class="collapse navbar-collapse">
                <div class="nav navbar-nav navbar-right">
                    <a href='./signup.php' class="btn navbar-btn btn-danger">Signup</a>
                </div>
            </div>

        </div>
    </div>

    <!-- Begin page content -->
    <div class="container" style="padding-top: 60px;">
        <div class="row">
            <div class="col-sm-12">
                <h4>
                    Welcome to talkie, An App that allows you to talk to your friends, whether they are online or not.
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="well-sm">
                    <h3>Already a user?
                        <small>Login with your credentials.</small>
                    </h3>
                    <?php if (isset($error_message)) { ?>
                        <p style="color: red;"><?php echo $error_message; ?></p>
                    <?php } ?>
                    <form action="./core/login_hander.php" method="post">
                        <label for="">Email</label>

                        <div class="margin_bottom">
                            <input placeholder="Email" class="form-control" required="" type="text" name="email"/>
                        </div>
                        <label for="">Password</label>

                        <div class="margin_bottom">
                            <input placeholder="Password" class="form-control" required="" type="password"
                                   name="password"/>
                        </div>
<!--                        <input type="hidden" name="mobile" />-->
                        <button type="submit" class="btn btn-danger btn-lg">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted credit">&copy 2014 Talkie</p>
    </div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>