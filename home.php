<?php
session_start();
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
}else{
    header("Location: ./");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Talkie</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./css/sticky-footer.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Wrap all page content here -->
<div id="wrap">

    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./">Talkie</a>
            </div>
            <div class="collapse navbar-collapse">
                <div class="nav navbar-nav navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome <?php echo $user['nickname'] ?><b
                                    class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-header">User Options</li>
                                <li class="divider"></li>
                                <li><a href="./logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <!-- Begin page content -->
    <div class="container" style="padding-top: 60px;">
        <div class="row">
            <div class="col-sm-6">
                <a href=""><h4>Offline Messages (No)</h4></a>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-sm-6">
                <h4>Online Contacts</h4>
                <div class="list-group">
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                    <a href="" class="list-group-item">E-papa</a>
                </div>
                <a href="" >All Online</a>
                <br />
                <a href="" >See All</a>
            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted credit">&copy 2014 Talkie</p>
    </div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>